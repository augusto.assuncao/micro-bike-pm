package pm.api.project.bicicleta;

/**
 * Bicicleta
 */
public class Bicicleta{

    private String id;
    private Integer code;
    private BicicletaStatus status;
    private String marca;
    private String modelo;
    private Integer ano;
    private String localizacao;

    public Bicicleta(String idValue, Integer codeValue, BicicletaStatus statusValue) {
        this.id = idValue;
        this.code = codeValue;
        this.status = statusValue;
    }

    public Bicicleta(String idValue, 
                    Integer codeValue, 
                    BicicletaStatus statusValue,
                    String marca,
                    String modelo,
                    Integer ano,
                    String localizacao) {
        this.id = idValue;
        this.code = codeValue;
        this.status = statusValue;
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.localizacao = localizacao;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getId() {
        return this.id;
    }

    public BicicletaStatus getStatus() {
        return this.status;
    }
    
    public void setStatus(BicicletaStatus status) {
        this.status = status;
    }

    public Integer getAno() {
        return ano;
    }
    public String getLocalizacao() {
        return localizacao;
    }
    public String getMarca() {
        return marca;
    }
    public String getModelo() {
        return modelo;
    }
    public void setAno(Integer ano) {
        this.ano = ano;
    }
    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}