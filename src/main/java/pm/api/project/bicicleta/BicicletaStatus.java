package pm.api.project.bicicleta;

public enum BicicletaStatus {
    NOVA,
    EM_USO,
    DISPONIVEL ,
    REPARO_SOLICITADO,
    EM_REPARO,
    APOSENTADA;
}
